from flask import Flask, request, jsonify
import psycopg2
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import os

app = Flask(__name__)
CORS(app)
app.secret_key = "supersecretkey"


# Configure the SQLAlchemy part of the app instance
app.config['SQLALCHEMY_DATABASE_URI'] = f"postgresql://{os.getenv('DB_USER')}:{os.getenv('DB_PASSWORD')}@{os.getenv('DB_HOST')}/{os.getenv('DB_NAME')}"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Create the SQLAlchemy db instance
db = SQLAlchemy(app)

# Define a User model


class User(db.Model):
    __tablename__ = 'your_table'  # Specify the table name
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    created_at = db.Column(db.DateTime, default=db.func.current_timestamp())


@app.route('/')
def index():

    return "Welcome to Flask-app."


@app.route('/fetch_users', methods=['GET'])
def list_users():
    users = User.query.all()
    users_list = [
        {
            "id": user.id,
            "name": user.name,
            "email": user.email,
            "created_at": user.created_at
        } for user in users
    ]
    return jsonify(users_list)


@app.route('/create_user', methods=['POST'])
def add_user():
    data = request.get_json()
    new_user = User(
        name=data['name'],
        email=data['email'],
    )
    db.session.add(new_user)
    db.session.commit()
    return jsonify({"message": "User added successfully!"}), 201


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
